FROM mambaorg/micromamba:0.27-focal-cuda-11.6.2

WORKDIR /day_1
RUN mkdir data
COPY --chown=$MAMBA_USER:$MAMBA_USER requirements.yml requirements.yml

# Micromamba is the light flavor of mamba, the cpp implementation of conda.
RUN micromamba install -f requirements.yml --yes && \
    micromamba clean --all --yes

COPY --chown=$MAMBA_USER:$MAMBA_USER code code

ENV wandb_key="Pass On Run"
CMD wandb login $wandb_key && \
    python code/training.py
