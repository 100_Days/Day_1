# Money-Saving HypergraphNN Collaborative Filtering

![Image Credit: Quanta Magazine](https://d2r55xnwy6nx47.cloudfront.net/uploads/2021/05/Homology_2880x1620_Lede.jpg) Image Credit: Quanta Magazine 
## The Why

When making recommendations you want speed, accuracy, and model simplicity. Speed and accuracy together keep your customers on your site and coming back, while simplicity drives down your development costs. As a graph neural network, this model handles feature engineering for you, shrinking your pipelines and your chances of a bug. Further, it helps increase revenue by rapidly providing desirable recommendations to your users.

The collaborative filtering technique involves taking observed business-user interactions mapping them into a latent space, and then inferring new businesses the user might enjoy from the latent representations. By using a graph-based model, where users and businesses are vertices in the graph and interactions are edges, we are able to more explicitly build the latent representations to reflect not only reviewed businesses but also businesses that are similar to reviewed businesses. This improves recommendation relevance.  


### Exploring simplifications of SOTA graph-based recommenders in search of more performant models.

In this project, we train a graph neural network to perform collaborative filtering using the Yelp! review dataset. If you need a refresher on the what, why, or how of graph neural networks, a great intro is available [here.](https://distill.pub/2021/gnn-intro/)

The network we propose comprises a series of simplifications and enhancements to the models of Xia, et.al. 2022, and others. In particular, we:
1. Allow the hypergraph embedding dimensionality to vary rather than fixing it at the graph embedding dimensionality.
2. Permit noisy negative samples to enhance both regularization and efficiency.
3. Use the same sub-network to process both the user and business embeddings.
4. Implement the layers with a classic batch dimension.

### Skills
* PyTorch
* Torchdata
* Graph Processing
* AWS
* GitLab CI/CD
* Docker
* Algorithm Optimization
  * The Hypergraph transforms were constructed to take maximum advantage of the NVIDIA A100 gpus on which they were trained.

### The Data

We use the Yelp! review dataset, it is available at https://www.yelp.com/dataset.

Since we are performing collaborative filtering, we only care about the existence of an interaction between a user and a business, not the nature of the interaction. Thus, all observed interactions are taken to be positive interactions. There exist psychology-based justifications for this simplification. The goal of collaborative filtering is to contrast between interactions that are similar to those observed and those that are different. Since observation of an interaction is assumed to imply positivity, to get our negative samples, we sample from the set of possible interactions and deem this sample negative. 

Businesses are checked for those with at least ten reviews, then users are checked for at least least ten reviews, it is acceptable if this procedure yields businesses with less than ten reviews in the dataset, because each business has been verified to have at least ten reviews. Our procedure differs from prior art where enforcing at least twenty reviews is common. But, this heightened sparsity is helpful to our training procedure.

The training set is created by holding out the last review for every user, and the validation set is comprised of 20% of the development set.

Number of Users: 111,434

Number of Businesses: 100,184

Number of Interactions: 3,111,228

Density: 2.7e-4


## Modules

### Light Graph Convolution Embedding

As proposed by Xia, et.al. 2022, the input layer embeds the nodes, then applies two light graph convolution layers. 

The light graph convolution was initially proposed in He, et.al. 2020, they postulated that the layer was more successful than the neural collaborative filtering of He, et.al. 2017 because the graph convolutions smooth the surface of the kernel (the function of the dot products). My belief is that it conveys geometric information in terms of closest nodes, and thereby creates a rich embedding that would be difficult to elucidate with gradient descent alone.

In our model, consistent with He, et. al. 2020, the graph for the entire dataset is used on every run in the light graph convolution layers. Then, only the representations needed for the mini-batch are processed by the rest of the model. Creating and storing the subgraphs for users and businesses needed in each minibatch is less time and memory efficient than our procedure.

### Hypergraph Transformer

Xia, et. al.'s 2022 hypergraph transformer uses attention to map the nodes into the edges of a hypergraph. Then the module applies a MLP to the hyperedges before using attention to map the hyperedges back to the nodes. The second attention layer uses the same keys and queries as produced in the first attention phase, but with roles reversed. This module maps the sparse nodes to a dense matrix representation, then transforms that representation rather than using less efficient graph-based mappings. Essentially the information is processed in a dense representation that is lighter to work with and therefore more efficient.

Critically, since the batch dimension represents the nodes of the subgraph that we are interested in predicting edges on, this module contracts over the batch dimension. Xia, et.al. treat each subgraph as a batch, this slows training when using small subgraphs. Worse, it confounds the effect of subgraph size with batch size. Since the model is compact, we are able to use a large batch size. Then, we chunk each batch into subgraphs, separating the effects of subgraph size and batch size.

Using the batch dimension like this may seem unnatural, but in this application, each batch represents a graph. In the context of image processing, it is common to diminish the height and weight dimensions while increasing the number of filters, and thereby combining pixels. Similarly, we aggregate nodes, and use a near-fully-connected graph to achieve a dense representation that has higher expressiveness and is easier to work with.

As a consequence on this contraction, it is necessary to ensure that a sufficiently large batch size is present so that the model benefits from a representative sample. Accordingly, we sample during prediction, yielding a random output. 

This image analogy is the inspiration for one of our tweaks. Xia, et. al. keep the dimension of the embedding the same for the node and hyper_edge representations. We allow the hyperedge representation to vary in size, and thereby take advantage of lessons learned in the computer vision community.


### Self-Supervised Solidity Augmentation

The following SolidityNetwork is inspired by Yu and Yin, et al. 2021. The Solidity network helps regularize the hyperedge and node representations by encouraging them to reflect the same grouping mechanics. This auxillary loss and network is useful because the primary loss maximizes the boundary between seen and unseen business-item interactions.

Our network differs from the preceding in both simplicity, and representativeness. In the attentions mechanisms, we store the $k^Tq$ vectors as one rather than two parameters to save matrix multiplications. As a consequence, the actual embedding information is hidden, and only the transition information between node and hyperedge is available. Luckily, the grouping information stored in this matrix is exactly what we are interested in. 


## The Model

While the model is influenced by Xia, et.al. 2022, the network topology is considerably simplified.

Rather than using separate hypergraph transformer sub-networks for user and business, we use one path for both, processing them in parallel.

Going all the way back to He, et.al. 2017 and their original neural collaborative filtering model, two paths were used because the two embeddings were assumed to be in different spaces. The necessity of this two-space assumption is questionable, and almost surely false here, where the embeddings are initially mixed together in the two light graph convolution layers.

This one path topology will be more efficient so long as the linear transformations don't require more than a 41% increase in size. Put another way, as long as 59% of the user and business embeddings can be processed in the same way, this topology will be more efficient. Further, each parameter will receive twice as strong of a signal during back-propagation, which should reduce training time. Comparing both topologies would require using a finer-grained search, and then doubling that search space. Due to constraints, we assume based on the success of the light graph convolution that processing both user and business with the same sub-network is better.

## Losses

We use the hard Bayesian personalized ranking loss as proposed by Rendle, et.al. 2009, due to its desirable theoretical justification. For the auxillary loss, we use a modified version of pairwise solidity loss as described by Xia, at. al. 2022. This loss is applied to the solidity network output and helps align similar embeddings.

We evaluate the final model using micro-aggregated recall and NDCG at 20, but use recall at 5 for cross-validation. We use a smaller recall for validation because only 100 candidate interactions are considered during validation, a number far smaller then during testing. The smaller candidate pool helps expedite training.


## Training

Unlike prior art, we do not filter out positive interactions when sampling negative ones. The probability of sampling a positive interaction is merely 2.7e-4, so our strategy acts as a nice regularizer. Additionally, it increases efficiency because we don't have to search the set of positive interactions at every negative interaction draw.

Inspired by Smith 2017, we use a triangular learning rate schedule, which peaks at the first quarter, then cosine anneals back to zero. We train the model for 200 epochs and allow for early stopping.

Information about particular training runs will be available soon. 


## Deployment

With the set of users and businesses fixed, the graph can be updated by adding new edges. Further, the embedding layer only provides each node with context from neighbors that are at most two hops away. So, if the entire graph is too large to work with in production, likely subgraphs could be processed instead. This would involve selecting the necessary neighbors for each of the $n$ most likely businesses, and only processing those businesses.

Additionally, the optimized Einstein summations can be switched out for ones that don't have the batch dimension.

## Issues

A noteworthy issue with the methods used to train and evaluate this model is the disparity in the distributions of the train, validation, and test sets. The model is trained on data that is 50% positive, validated on data that is 1% positive, and tested on data that has an even smaller proportion of positive values, then deployed on data that depending on strategy may be 0% positive. Since the training data is biased, and the model aggregates interactions to produce a final solution, there is the potential for the model to learn a skewed distribution.

A potential solution could be to use a lower proportion of positive interactions during training and then reweighting to account for imbalance, but this would be challenging with the Bayesian personalized ranking loss. However, this loss by definition helps compensate for imbalance somewhat.

## References
Xiangnan He, Lizi Liao, Hanwang Zhang, Liqiang Nie, Xia Hu, and Tat-Seng Chua. 2017. Neural collaborative filtering. https://arxiv.org/pdf/1708.05031.pdf

Xiangnan He, Kuan Deng, Xiang Wang, et al. 2020. Lightgcn: Simplifying and powering graph convolution network for recommendation. https://arxiv.org/pdf/2002.02126.pdf

Steffen Rendle, Christoph Freudenthaler, Zeno Gantner and Lars Schmidt-Thieme. 2009. BPR: Bayesian Personalized Ranking from Implicit Feedback. https://arxiv.org/pdf/1205.2618.pdf

Leslie N. Smith. 2017. Cyclical Learning Rates for Training Neural Networks. https://arxiv.org/abs/1506.01186.pdf

Junliang Yu, Hongzhi Yin,Jundong Li, Qinyong Wang, Nguyen Quoc Viet Hung, and Xiangliang Zhang. 2021. Self-Supervised Multi-Channel Hypergraph Convolutional Network for Social Recommendation. https://arxiv.org/pdf/2101.06448.pdf

Lianghao Xia, Chao Huang, Chuxu Zhang. 2022. Self-Supervised Hypergraph Transformer for Recommender Systems. https://arxiv.org/pdf/2207.14338.pdf