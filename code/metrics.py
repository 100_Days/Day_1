import torch
import torch.nn as nn
import torch.nn.functional as F
import torchmetrics


class PairWiseSolidityLoss(nn.Module):

    def __init__(self, p, batch_size):
        """
        ARGS:
            p: The probability of including an interaction in the loss.
        """
        super().__init__()
        self.register_buffer('p', torch.tensor([p]*(batch_size//2)))


    def forward(self, hyp, emb):
        """
        ARGS:
            hyp - The hypergraph-sourced solidity scores.
            emb - The embedding-sourced solidity scores.
        """
        hyp = hyp.mean(0)
        hyp = hyp[..., 0] - hyp[..., 1] # pos - neg
        emb = emb[..., 0] - emb[..., 1]
        loss = F.relu(1 - hyp*emb)
        return torch.mean(loss * self.p.bernoulli())


class HardBPR(nn.Module):
    """
    A hard version of the Bayesian Personalized Ranking initially
    proposed by Rendle et al., 2009.
    """

    def forward(self, pos, neg):
        dif = pos.unsqueeze(1) - neg.unsqueeze(2)
        return F.relu(1-dif).mean()


class RecallAtK(torchmetrics.Metric):
    """
    Micro-aggregated recall at k; uses score size rather than a threshold.
    """

    def __init__(self, k):
        super().__init__()
        self.k = k
        self.add_state('correct', default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state('total', default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, pred, target):
        """
        ARGS:
            pred: [batch, n_nodes, scores]
            target: [batch, n_nodes, t_idxs] t_idxs should contain
                the indices of the target businesses.
        """
        _, idxs = torch.topk(pred, self.k, dim=2)
        # correct = total number of targets per top spot
        correct = torch.sum(idxs.unsqueeze(2) == target.unsqueeze(3))
        self.correct = self.correct + correct
        self.total = self.total + target.numel()

    def compute(self):
        return self.correct.float() / self.total 


class NDCGAtK(torchmetrics.Metric):
    """
    Micro-averaged implementation of NDCG at k. Assumes less than k targets. 
    """

    def __init__(self, k):
        super().__init__()
        self.k = k
        self.register_buffer('discount', 1. / torch.log2(torch.arange(2, k+2)))
        self.add_state('real', torch.zeros(k, dtype=torch.int))
        self.add_state('ideal', torch.zeros(k))
        self.add_state('total', torch.tensor(0))

    def update(self, pred, target):
        """
        ARGS:
            pred: [batch, n_nodes, scores]
            target: [batch, n_nodes, t_idxs] t_idxs should contain
                the indices of the target business.
        """
        # relevance = total number of targets per top spot
        _, idxs = torch.topk(pred, self.k, dim=2)
        relevance = (target.unsqueeze(2) == idxs.unsqueeze(3)).sum([0, 1, 3])
        self.real = self.real + relevance

        n_inters = pred.size(0)*pred.size(1)
        self.ideal[:target.size(2)] = self.ideal[:target.size(2)] + n_inters
        self.total = self.total + n_inters

    def compute(self):
        real_rel =  self.real.float() / self.total
        ideal_rel = self.ideal.float() / self.total
        dcg = torch.inner(2.**real_rel - 1., self.discount)
        idcg = torch.inner(2.**ideal_rel - 1., self.discount)
        return dcg/idcg