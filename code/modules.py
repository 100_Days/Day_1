import math
import opt_einsum as oe
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch_geometric.nn as gnn


class LightGCEmbedding(nn.Module):
    """
    An embedding layer that takes advantage of He, et.al.'s [2020]
    Light Graph Conv to create a rich embedding aggregated from the
    neighbor's embeddings; as proposed by Xia, et.al. [2022].
    """
    def __init__(self, n_users, n_bizs, d_model, edge_idxs):
        super().__init__()
        self.user_emb = nn.Embedding(n_users, d_model)
        self.biz_emb = nn.Embedding(n_bizs, d_model)
        self.light_gc0 = gnn.LGConv()
        self.light_gc1 = gnn.LGConv()

        self.n_users = n_users
        self.register_buffer('user_nodes', torch.arange(n_users))
        self.register_buffer('biz_nodes', torch.arange(n_bizs))
        self.register_buffer('edges', edge_idxs)

    def forward(self, user, biz):
        emb0 = torch.cat([self.user_emb(self.user_nodes),
                          self.biz_emb(self.biz_nodes)])
        emb1 = self.light_gc0(emb0, self.edges)
        emb1 = self.light_gc1(emb1, self.edges)
        
        user = emb0[user] + emb1[user]
        biz = emb0[self.n_users+biz] + emb1[self.n_users+biz]
        return user, biz


class ReLUSq(nn.Module):

    def forward(self, ipt):
        opt = F.relu(ipt)
        return opt*opt


class HypergraphTransformer(nn.Module):
    """
    This implementation of a hypergraph transformer takes
    advantage of opt_einsum to produce optimal multi-head
    self-attn transforms. In particular, the einsum
    expression is optimized for training, but a different
    einsum expression can be substituted for deployment.
    """
    
    def __init__(self, batch_size, n_nodes, nhe, d_model,
                 dhe, n_heads=4, dropout=0.1):
        super().__init__()
        assert d_model % n_heads == 0, 'n_heads must divide embedding dim.'
        assert dhe % n_heads == 0, 'n_heads must divide hyper-embedding dim.'
        self.n_heads = n_heads
        
        self.d_model = d_model
        self.dhe = dhe
        self.n_nodes = n_nodes
        self.nhe = nhe
        
        self.batch_size = batch_size
        self.KtQ = nn.Parameter(torch.empty(nhe, n_heads, d_model))
        self.V = nn.Parameter(torch.empty(dhe, d_model))
        
        self.register_parameter('KtQ', self.KtQ)
        self.register_parameter('V', self.V)
        self.norm_in = nn.InstanceNorm1d(nhe)
        self.norm_out = nn.InstanceNorm1d(n_nodes)

        # use Kaiming init, but implement manually because torch computes wrong
        bound = 1./math.sqrt(d_model)
        h_bound = 1./math.sqrt(dhe)
        nn.init.uniform_(self.KtQ, a=-bound, b=bound)
        nn.init.uniform_(self.V, a=-h_bound, b=h_bound)

        in_size = (batch_size, n_nodes, d_model)
        self.cntrx = oe.contract_expression('bgj,hij->bghi',
                                            in_size, self.KtQ.size())

        ktq_size = (batch_size, n_nodes, nhe, n_heads)
        hyp_size = (batch_size, nhe, dhe)
        V0_size = (n_heads, dhe//n_heads, d_model)
        V1_size = (n_heads, d_model//n_heads, dhe)
        
        self.attn_in = oe.contract_expression('bcn,imn,bghi->bhim',
                                              in_size, V0_size, ktq_size)    
        self.attn_out = oe.contract_expression('bdp,iqp,bghi->bgiq',
                                               hyp_size, V1_size, ktq_size)

        hhgl = nn.Sequential(
            nn.Linear(dhe, dhe),
            nn.Dropout(p=dropout),
            ReLUSq(),
            nn.InstanceNorm1d(nhe))
        self.hhgn = nn.Sequential(*(hhgl for _ in range(2)))

    def forward(self, ipt):
        # Names reference canonical notation, rather
        # than the nomenclature from Xia, et. al.
        ktq = self.cntrx(ipt, self.KtQ, backend='torch')
        hx = self.attn_in(ipt, self.V.view(self.n_heads, -1, self.d_model),
                          ktq, backend='torch')
        hx = self.norm_in(hx.view(-1, self.nhe, self.dhe))
        hy = self.hhgn(hx) + hx

        opt = self.attn_out(hy, self.V.t().view(self.n_heads, -1, self.dhe),
                            ktq, backend='torch')
        opt = self.norm_out(opt.view(-1, self.n_nodes, self.d_model))
        if self.training:
            return opt+ipt, ktq
        else:
            return opt+ipt


def sanity_check(pre, post, post_name):
    mask = post.isnan()
    if not mask.any().item():
        return None

    print(post_name)
    print('\tpost stride:', post.stride())
    print('\tpost size:', post.size())
    print('\tpre stride:', pre.stride())
    print('\tpre size:', pre.size())
    
    if pre.size() == post.size():
        print('\tPROBLEM', pre[mask])


class SolidityNetwork(nn.Module):
    """
    This network is inspired by, but more efficient than, the local-global
    self-augmented meta-network from Xia, et.al., 2022. As in the preceding,
    this network predicts solidity scores.
    """

    def __init__(self, n_nodes):
        super().__init__()
        self.lyr = nn.Sequential(
            nn.Linear(n_nodes, n_nodes),
            nn.Sigmoid())

    def forward(self, ktq_user, ktq_biz):
        s = torch.einsum('ijk,ijk->ij',
                         ktq_user.mean(-1),
                         ktq_biz.mean(-1))
        return self.lyr(s) # batch, n_nodes