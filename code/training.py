from datetime import datetime
import functools
import json
import numpy as np

from modules import LightGCEmbedding, HypergraphTransformer, SolidityNetwork
from metrics import PairWiseSolidityLoss, HardBPR, RecallAtK, NDCGAtK
import optuna
import pytorch_lightning as pl
from pathlib import Path

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchdata.datapipes.iter import FileLister, IterableWrapper

import wandb
import warnings
warnings.simplefilter('ignore', UserWarning)

# I don't believe in seeding my models. If the model can't work well
# for multiple initializations, then it is a bad model.
# Seeding is only used for the train-test split, allowing for reproducible
# comparisons.
PATH = '/day_1/data'
N_USERS = 111_434
N_BIZS = 100_184

BATCH_SIZE = 8192
SEED_SEQ = np.random.SeedSequence(185276279063290614694355344216212697936)
graph_edges = None

class YelpDataModule(pl.LightningDataModule):

    def __init__(self, path, n_users, n_bizs, batch_size, n_nodes=None):
        super().__init__()
        self.path = path
        self.n_users = n_users
        self.n_bizs = n_bizs

        self.n_nodes = n_nodes
        self.n_batches = None if n_nodes is None else batch_size // n_nodes
        self.rng = np.random.default_rng(SEED_SEQ)

        # Normally, I would handle much of this preprocessing
        # before hand to save time, but the torchdata api is
        # new and I wanted to explore it.

        # Load data and collect admissible subset
        loading_pipe = (FileLister(root=self.path)
            .open_files(mode='b')
            .load_from_zip()
            .readlines(return_path=False, decode=True)
            .map(self.read_and_extract)
            .groupby(lambda x: x[1], buffer_size=-1) # biz
            .filter(lambda x: len(x)>9)
            .unbatch()
            .groupby(lambda x: x[0], buffer_size=-1) # user
            .filter(lambda x: len(x)>9))

        # Replace ids with indices, and mark the last interaction for
        # each user to be held out. Finally, perform train-test split.
        train_pipe, test_pipe = (loading_pipe
            .enumerate()
            .map(self.standardize)
            .unbatch()
            .groupby(lambda x: x[1], buffer_size=-1)
            .enumerate()
            .map(functools.partial(self.standardize, user=False))
            .unbatch()
            .demux(2, lambda x: x[2], buffer_size=-1))

        self.test_set = list(test_pipe
            .groupby(lambda x: x[0], buffer_size=-1)
            .map(lambda x: (x[0][0], torch.tensor([t[1] for t in x]))))


        # Create user-business interaction graph. 
        fork0, fork1 = train_pipe.map(lambda x: x[:2]).fork(2, buffer_size=-1)
        global graph_edges
        graph_edges = torch.tensor(list(fork0)).t()

        # Finally, save the data to be read as needed.
        train, val = (fork1
            .demux(2, lambda x: self.rng.choice(2, size=1, p=[0.8, 0.2])[0],
                   buffer_size=-1))
        
        self.train_set = list(train)[:16384]
        self.val_set = list(val)[:16384]

    def setup(self, stage):
        if stage == 'fit':
            assert self.n_nodes is not None
            # batch halved to sample neg interactions
            self.train_pipe = (IterableWrapper(self.train_set)
                .shuffle()
                .batch(self.n_nodes//2, drop_last=True)
                .map(lambda x: (torch.tensor([t[0] for t in x]),
                                torch.tensor([t[1] for t in x]))))

            self.val_pipe = (IterableWrapper(self.val_set)
                .shuffle()
                .batch(self.n_nodes, drop_last=True)
                .map(lambda x: (torch.tensor([t[0] for t in x]),
                                torch.tensor([t[1] for t in x]))))
                                
        elif stage == 'validate':
            assert self.n_nodes is not None
            self.val_pipe = (IterableWrapper(self.val_set)
                .shuffle()
                .batch(self.n_nodes, drop_last=True)
                .map(lambda x: (torch.tensor([t[0] for t in x]),
                                torch.tensor([t[1] for t in x]))))

        elif stage == 'test':
            assert self.n_nodes is not None
            # batch halved because 
            self.test_pipe = (IterableWrapper(self.test_set)
                .batch(self.n_nodes, drop_last=True)
                .map(lambda x: (torch.tensor([t[0] for t in x]),
                                torch.stack([t[1] for t in x]))))

        else:
            raise ValueError('stage must be one of "fit", '
                             '"validate", or "test".')

    def train_dataloader(self):
        assert self.n_batches is not None
        return DataLoader(self.train_pipe, batch_size=self.n_batches,
                          drop_last=True, pin_memory=True)

    def val_dataloader(self):
        assert self.n_batches is not None
        return DataLoader(self.val_pipe, batch_size=self.n_batches,
                          drop_last=True, pin_memory=True)

    def test_dataloader(self):
        assert self.n_batches is not None
        return DataLoader(self.test_pipe, batch_size=self.n_batches,
                          pin_memory=True)

    def read_and_extract(self, line):
        """
        Loads the JSON and extracts business_id, user_id, and date.
        The structure of the data prevents us from loading an entire
        file at once.
        """
        raw = json.loads(line)
        date = datetime.fromisoformat(raw['date'])
        return raw['user_id'], raw['business_id'], date

    def standardize(self, idx_group, user=True, k=1):
        """
        Replaces user/business with index, and if i == 0, marks the last
        k interactions. If user==True, then users are indexed; else
        businesses are indexed. 
        """
        idx, group = idx_group
        if user:
            last_k = sorted(group, key=lambda x: x[2], reverse=True)[:k]
            return [(idx, x[1], 1) if x in last_k else (idx, x[1], 0)
                        for x in group]
        else:
            return [(x, idx, y) for x, _, y in group]


class YelpModel(pl.LightningModule):

    def __init__(self, n_users, n_bizs, batch_size, n_nodes,
                 d_model, hgts, edge_idxs, learning_rate=0.01,
                 sld_reg=0.1, L2_reg=0.4, dropout=0.2):
        super().__init__()
        self.save_hyperparameters()
        
        self.n_users = n_users
        self.n_bizs = n_bizs
        self.n_batches = batch_size // n_nodes
        self.n_nodes = n_nodes

        self.d_model = d_model
        self.L2_reg = L2_reg
        self.lr = learning_rate
        self.sld_reg = sld_reg
        
        self.lgc_emb = LightGCEmbedding(n_users, n_bizs, d_model, edge_idxs)
        self.sld_net = SolidityNetwork(n_nodes)
        self.hgt_net = nn.ModuleList()
        for dhe, nhe in hgts:
            hgt = HypergraphTransformer(self.n_batches, n_nodes, nhe,
                                        d_model, dhe, dropout=dropout)
            self.hgt_net.append(hgt)
        
        self.pwsl = PairWiseSolidityLoss(0.1, batch_size)
        self.bpr = HardBPR()
        self.recall_at5 = RecallAtK(5 )
        self.recall_at20 = RecallAtK(20)
        self.ndcg_at20 = NDCGAtK(20)
        
    def forward(self, user):
        biz = torch.arange(self.n_bizs, device=self.device)
        # add extra bizs to ensure equal sized chunks
        add_len = user.numel() - self.n_bizs%user.numel()
        clean_edge = torch.randint(self.n_bizs, (add_len,),
                                   device=self.device)
        biz = torch.cat([biz, clean_edge]).view(-1, *user.size())

        x_user, biz_emb = self.lgc_emb(user, biz)
        for hgt in self.hgt_net:
            x_user = hgt(x_user)

        pred = torch.empty(
            *user.size(), self.n_bizs+add_len, device=self.device)
        for j, b in enumerate(biz_emb.split(1)):
            b = b.squeeze()
            for hgt in self.hgt_net:
                b = hgt(b)
            s = slice(j*user.numel(), (j+1)*user.numel())
            pred[..., s] = torch.inner(x_user, b.view(user.numel(), -1))
            
        return pred[:, :self.n_bizs]

    def training_step(self, batch, batch_idx):
        user, biz = batch
        neg_user = torch.randint(self.n_users, user.size(), device=self.device)
        neg_biz = torch.randint(self.n_bizs, biz.size(), device=self.device)

        user = (torch.stack([user, neg_user], dim=2)
            .view(user.size(0), -1)) # batch, n_nodes
        biz = (torch.stack([biz, neg_biz], dim=2)
            .view(biz.size(0), -1)) # batch, n_nodes
        
        x_user, x_biz = self.lgc_emb(user, biz)
        s_emb = torch.einsum('bij,bij->bi', x_user, x_biz)
        s_hyp = torch.empty(len(self.hgt_net), *x_user.size()[:2],
                            device=self.device)
        for i, hgt in enumerate(self.hgt_net):
            x_user, ktq_user = hgt(x_user)
            x_biz, ktq_biz = hgt(x_biz)
            s_hyp[i] = self.sld_net(ktq_user, ktq_biz)

        s_emb = s_emb.view(-1, 2)
        s_hyp = s_hyp.view(s_hyp.size(0), -1, 2)
        aux_loss = self.pwsl(s_hyp, s_emb)
        y = torch.einsum('bij,bij->bi', x_user, x_biz)

        y = y.view(y.size(0), -1, 2)
        loss = self.bpr(y[..., 0], y[..., 1]) + self.sld_reg*aux_loss
        self.log('loss', loss, prog_bar=True)
        return loss
        
    def validation_step(self, batch, batch_idx):
        user, biz = batch
        biz = torch.cat(
            dim=1, tensors=[biz, torch.randint(self.n_bizs,
                                               (biz.size(0), 99*biz.size(1), ),
                                               device=self.device)])

        x_user, biz_emb = self.lgc_emb(user, biz)
        for hgt in self.hgt_net:
            x_user = hgt(x_user)

        # each chunk is the size of a training input
        chunks = biz_emb.split(user.size(1), dim=1)
        pred = torch.empty(*user.size(), len(chunks), device=self.device)
        for k, b in enumerate(chunks):
            for hgt in self.hgt_net:
                b = hgt(b)
            pred[..., k] = torch.einsum('bij,bij->bi', x_user, b)

        self.recall_at5(pred, torch.zeros_like(user).unsqueeze(2))
        self.log('Recall@5', self.recall_at5, on_epoch=True)

    def test_step(self, batch, batch_idx):
        user, target_biz = batch
        pred = self(user)
        self.recall_at20(pred, target_biz)
        self.ndcg_at20(pred, target_biz)

        self.log('Test Recall@20', self.recall_at20, on_epoch=True)
        self.log('Test NDCG@20', self.ndcg_at20, on_epoch=True)

    def configure_optimizers(self):
        stop = self.trainer.max_epochs
        peak = self.trainer.max_epochs//4
        opt = torch.optim.Adam(self.parameters(), lr=self.lr,
                               weight_decay=self.L2_reg)

        sch = torch.optim.lr_scheduler.ChainedScheduler([
            torch.optim.lr_scheduler.LinearLR(opt, start_factor=self.lr/10,
                                              total_iters=peak),
            torch.optim.lr_scheduler.CosineAnnealingLR(opt, stop-peak)])
            
        return [opt], [sch]


def objective(trial):
    n_nodes = trial.suggest_int('n_nodes', 7, 10)
    d_model = trial.suggest_int('d_model', 5, 7)
    n_layers = trial.suggest_int('n_layers', 2, 4)

    hgts = []
    nhe, dhe = n_nodes, d_model
    for i in range(n_layers):
        nhe = trial.suggest_int(f'nhe{i}', 3, nhe)
        dhe = trial.suggest_int(f'dhe{i}', dhe, 10)
        hgts.append((2**nhe, 2**dhe))

    # Changing data attributes allows for reusing one data
    # module during training, reducing loads.
    n_nodes = 2**n_nodes
    d_model = 2**d_model
    data.n_nodes = n_nodes
    data.n_batches = BATCH_SIZE // n_nodes

    model = YelpModel(N_USERS, N_BIZS, BATCH_SIZE, n_nodes,
                      d_model, hgts, graph_edges)
    name = f'{n_nodes}n_{d_model}d_{n_layers}l'
    trial.set_user_attr('model_name', name)
    logger = pl.loggers.WandbLogger(
        name=name, project='GraphRec', log_model='all')
    
    callbacks = [
        pl.callbacks.EarlyStopping(monitor='Recall@5', mode='max', patience=40),
        pl.callbacks.ModelCheckpoint(monitor='Recall@5', mode='max')]
    trainer = pl.Trainer(max_epochs=2, logger=logger,
                         accelerator='gpu', devices=1,
                         callbacks=callbacks,
                         enable_progress_bar=False)

    trial.set_user_attr('run_id', logger.version)
    trainer.fit(model, data)
    wandb.finish()
    return trainer.callback_metrics['Recall@5']

if __name__ == "__main__":
    print("Starting the hyperparameter search.")
    data = YelpDataModule(PATH, N_USERS, N_BIZS, BATCH_SIZE)
    study = optuna.create_study(
        study_name='gnn_rec_study', direction='maximize')
    study.optimize(objective, n_trials=1)

    best_run_id = study.best_trial.user_attrs['run_id']
    logger = pl.loggers.WandbLogger(
        name=study.best_trial.user_attrs['model_name'],
        id=best_run_id, project='GraphRec', resume=True)

    to_best_model = (logger
        .use_artifact(f'cgueli/GraphRec/model-{best_run_id}:best_k', 'model')
        .download())
    
    best_model = YelpModel.load_from_checkpoint(
        Path(to_best_model) / 'model.ckpt')
    data.n_nodes = best_model.n_nodes
    data.n_batches = best_model.n_batches

    tester = pl.Trainer(logger=logger,
                         accelerator='gpu', devices=1,
                         enable_progress_bar=False)
    tester.test(best_model, data)
    