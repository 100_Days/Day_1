#! /bin/sh

ip=""
while getopts "t:" flag; do
  case "${flag}" in
    t) ip="${OPTARG}" ;;
    ?) echo "t provides the target ip address."
       exit 1 ;;
  esac
done

cd ~/'100 Days'
wandb_key=`cat wandb_key.txt`
aws_cred=`cat aws_cred.csv`

ssh -T -i ~/.ssh/LambdaCloudSSH.pem ubuntu@${ip} << EOL

  echo "Downloading data"
  mkdir data
  aws configure import --csv "$aws_cred"
  aws s3 cp s3://100-days-of-data/Day_1 ./data --recursive --profile LambdaCloudAuto

  sudo docker run --name "training" \
    --gpus "all" -a stdout \
    -e wandb_key=$wandb_key \
    --mount type=bind,source=/home/ubuntu/data,target=/day_1/data,readonly \
    chadgueli/100days:day_1

EOL
echo "Hyperparameter search complete, exiting Lambda Cloud!"
